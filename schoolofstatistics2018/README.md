# IN2P3 School Of Statistics

![SoS](./logo_SOS.png)

## Summary

The sixth edition of the IN2P3 School Of Statistics gives an overview of the concepts and tools used in particle physics, astro-particle physics and cosmology when probabilities and statistics come to play.

This school is targeted towards PhD students and towards senior physicists, aiming at extending their knowledge and skills in the field of statistical tools and frameworks developed for their fields.

The school combines lectures and hands-on sessions. The lectures are subdivided into three parts: a part reminding the fundamental concepts used in Probabilities, Statistics and Hypothesis testing applied to physics analysis; a part focusing on the presentation of the concepts and basics of most popular multivariate techniques; and a part dedicated to actual multivariate tools and machine learning.

All lectures will be given in English.

The School Of Statistics is supported by CNRS/IN2P3.

## Repository organization

| Directory | Description |
| --- | --- |
| BasicsConcepts | Reminder about statistics |
| IntervalsLimits | Reminder about tests, intervals, LL |
| MachineLearning | Introduction to machine learning |
| TMVA | Introduction to Multivariate Data Analysis with TMVA |
| HandsOn | Hands on session |
| MLatHEP | Machine learning for High Energy physics |
| MLAstro | Machine learning for astronomy |

## Links

Useful links:

- [Indico edition 2018](https://indico.in2p3.fr/event/16931)
- [CNRS Website](https://sos.in2p3.fr/modeles/une.htm): all materials from previous editions are available here
